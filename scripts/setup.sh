#!/bin/sh
index=0
cmd="fio --thread --direct=1 --rw=write --norandommap --ioengine=libaio --group_reporting --bs=16M --iodepth=32 --name=drive0 --size=${FILESIZE}G"
create=0

for i in $NVMES; do    
    mkdir -p /data${index}
    echo "mounting $i"
    mount -o noatime,nodiratime,largeio $i /data${index}

    j=0
    while [ "$j" -lt "$NUM_FILES_PER_NVME" ]; do
	file="/data${index}/data${j}"
	echo "checking $file"
	if [ ! -f $file ]; then
	    echo "file $file not exist"
            cmd="${cmd} --filename=${file}"
            create=$(( create + 1 ))
	fi
	j=$(( j + 1 ))
    done
    index=$(( index + 1 ))
done

if [ $create -gt 0 ]; then
    echo "$cmd"
    $cmd
fi
