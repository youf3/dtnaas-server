#!/bin/sh
for i in $IPERF3_PORTS; do 
    in_use=$( netstat -tnap|grep ":$i " )
    echo in_use $in_use
    if [ "$in_use" == "" ]; then
        iperf3 -s -p $i
        break
    fi
 done