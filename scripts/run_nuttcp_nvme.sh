#!/bin/sh
index=0
for i in $NUTTCP_NVME_PORTS; do    
    in_use=$( netstat -tnap|grep ":$i " )
    echo in_use $in_use
    if [ "$in_use" == "" ]; then
        dport=$(( i + 1000 ))
        nuttcp -S -P $i -p $dport --nofork &
        resvp_pid=$(echo $!)
        echo "port $i reserved with pid ${resvp_pid}"
        sleep 2
	
        if ! kill -0 $resvp_pid > /dev/null; then
            echo "other prcess took the port $i"
            index=$(( index + 1 ))
            continue
        fi

        nvme_index=0        
        for j in $NVMES; do
            z=$(( index / NUM_FILES_PER_NVME ))
            datanum=$(( index%NUM_FILES_PER_NVME ))
            if [ $z -eq $nvme_index ]; then
		echo "z : $z, index = $index, nfpn = $NUM_FILES_PER_NVME"                
                NVMe=$j
                break
            fi
            nvme_index=$(( nvme_index + 1 ))
        done        
        
        mkdir data
        echo "mounting $NVMe"
        mount -o noatime,nodiratime,largeio $NVMe data

        if [ "$?" != "0" ]; then
            echo "Cannot mount $NVMe. Exiting.."
            exit -1
        fi

        infile="data/data${datanum}"
        outfile="data/data${datanum}.rec"

        if [ ! -f $infile ]; then
            echo "cannot find file $infile"
            echo "fio --thread --direct=1 --rw=write --norandommap --ioengine=libaio --group_reporting --bs=16M --iodepth=32 --name=drive0 --size=${FILESIZE}G --filename=$infile"
            fio --thread --direct=1 --rw=write --norandommap --ioengine=libaio --group_reporting --bs=16M --iodepth=32 --name=drive0 --size=${FILESIZE}G --filename=$infile
        fi

	echo "killing resv pid : ${resvp_pid}"
	kill -9 ${resvp_pid}
	kill -9 ${resvp_pid}
	sleep 1
        echo "nuttcp -S -1 -P $i -p $dport --nofork -sdz -l8m < $infile > $outfile"
        nuttcp -S -1 -P $i -p $dport --nofork -sdz -l8m < $infile > $outfile
        rm outfile
        umount data
        break
    fi 
    index=$(( index + 1 ))
 done
