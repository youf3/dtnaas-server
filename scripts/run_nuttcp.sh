#!/bin/sh
for i in $NUTTCP_MEM_PORTS; do 
    echo $i
    in_use=$( netstat -tnap|grep ":$i " )
    echo in_use $in_use
    if [ "$in_use" == "" ]; then
        dport=$(( i + 1000 ))
        echo "nuttcp -S -P $i -p $dport --nofork"
        nuttcp -S -P $i -p $dport --nofork
        break
    fi
 done