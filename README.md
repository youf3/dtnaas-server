## Dockerfile and scripts for deploying DTN-as-a-Service testpoint server

This repository contains scripts for deploying the DTN-as-a-Service testpoint server. It consists of iperf3 and nuttcp servers to accept tests from remote DTN.

To start deploying the servers, edit the .env file for your need.

Note: Make sure to backup your data in NVMe devices as the server fills them with testing data.

To find out which NVMe to use for the best performance for a specific NIC, run bootstrap.sh to identify and prepare NVMes that belong to the same NUMA with specific NIC.
